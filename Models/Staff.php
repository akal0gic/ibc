<?php

namespace TestTask\Models;

/**
 * атрибуты объекта
 * @property int $id            - primary key
 * @property str $full_name     - Ф.И.О сотрудника
 * @property str $position      - Должность
 * @property str $sex           - Пол
 * @property str $birthday      - Дата рождения
 * @property str $passport      - Паспорт (номер и серия)
 * @property str $phone         - Мобильный телефон
 * @property str $email         - Адрес электронной почты
 * @property str $deleted       - Флаг удаленных записей
 */
class Staff extends \TestTask\Library\ActiveRecord {

  /** реализовывает логирование измененых атрибутов */
  use \TestTask\Library\ARB\LogChanges;

/** реализовывает безопасное удаление */
  use \TestTask\Library\ARB\SafeDelete;

  /** @var \PDO default connection */
  protected $_db;
  protected $_table_name = 'staff';

  function __construct() {
    parent::__construct();

    # атрибуты объекта 
    $this->defineAttributes([
        'id',
        'full_name',
        'position',
        'sex',
        'birthday',
        'passport',
        'phone',
        'email',
        'deleted' => 'N',
    ]);

    $this->logChangesTakeSnapshot();
  }

  public function attributeLabels() {
    return [
        'full_name' => 'Ф.И.О.',
        'position' => 'Должность',
        'sex' => 'Пол',
        'birthday' => 'Дата рождения',
        'passport' => 'Паспортные данные',
        'phone' => 'Телефон',
    ];
  }

  /** Правила описывающие атрибуты объекта */
  public function rules() {
    return [
        # sys
        ['deleted', '\Validators\RegExp', 'pattern' => '~^[Y|N]{1}$~'],
        # def form
        ['full_name, position, phone, email', '\Validators\Required', 'message' => 'Укажите &laquo;{attribute}&raquo;'],
        ['full_name', '\Validators\RegExp', 'pattern' => '~^\S+\s{1}\S+\s{1}\S+$~', 'message' => 'Не верный формат поля &laquo;{attribute}&raquo; используйте "Фамилия Имя Отчество"'],
        ['position', '\Validators\RegExp', 'pattern' => '~^.{1,255}$~'],
        ['sex', '\Validators\RegExp', 'pattern' => '~^[M|F]{1}$~'],
        ['birthday', '\Validators\DateTime'],
        ['passport', '\Validators\RegExp', 'pattern' => '~^\d{4}\s*\d{6}$~'],
        ['phone', '\Validators\Phone'],
        ['email', '\Validators\RegExp', 'pattern' => '/^[\w\.-]+@[\w\.-]+$/'],
    ];
  }

  public function search() {
    $sql = "SELECT * FROM `{$this->_table_name}`  WHERE `deleted` = 'N' ORDER BY  `position`, `full_name`";
    $result = $this->_db->query($sql)->fetchAll(\PDO::FETCH_CLASS);

    return $result;
  }

  public function beforeSave() {
    # это ужас, но мне надоело писать свой фреймфорк для реализации одного CRUD`а
    if (!is_null($this->passport)) $this->passport = str_replace(' ', '', $this->passport);
    $this->phone = preg_replace('#[\D]*#', '', $this->phone);

    parent::beforeSave();
  }

}
