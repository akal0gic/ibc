<?php

namespace TestTask\Models;

/**
 * Шлюз к данным таблицы  log_model_changes
 * @see http://martinfowler.com/eaaCatalog/tableDataGateway.html
 *
 */
class LogModelChangesGateway {

  private static $_table_name = 'log_model_changes';

  public static function get($table, $record_id = false) {
    $db = \Registry::get('db');

    $sql_where = [];
    if ($record_id) $sql_where[] = 'AND `' . static::$_table_name . '`.`record_id` = :record_id';

    $sql = 'SELECT `' . static::$_table_name . '`.*, `' . $table . '`.`full_name`'
            . ' FROM `' . static::$_table_name . '` '
            . ' LEFT JOIN  `' . $table . '` ON (`' . $table . '`.`id` = `log_model_changes`.`record_id`)'
            . ' WHERE `table` = "' . $table . '" ' . implode(' ', $sql_where)
            . ' ORDER BY `' . static::$_table_name . '`. `id` DESC ';

    $pdo = $db->prepare($sql);
    if ($record_id) $pdo->bindParam(':record_id', $record_id);
    $pdo->execute();

    return $pdo->fetchAll(\PDO::FETCH_OBJ);
  }

}
