<?php

namespace Validators;

class DateTime extends Validator {

  public $message = 'Не верный формат формат даты';
  public $allowEmpty = true;

  public function validateAttribute() {
    $value = $this->getAttributeValue();
    if ($this->allowEmpty && $this->isEmpty($value)) return;

    try {
      $dt = (!is_numeric($value) ? new \DateTime($value) : (new \DateTime())->setTimestamp($value));
    } catch (\Exception $exc) {
      $this->addError($this->message);
    }
  }

}
