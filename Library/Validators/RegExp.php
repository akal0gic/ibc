<?php

namespace Validators;

class RegExp extends Validator {

  public $message = 'Не верный формат &laquo;{attribute}&raquo;';
  public $allowEmpty = true;
  public $pattern = null;

  public function validateAttribute() {
   
    if ($this->pattern === null)
        throw new \Exception('The "pattern" property must be specified with a valid regular expression.');

    $value = $this->getAttributeValue();

    if ($this->allowEmpty && $this->isEmpty($value)) return;

    if (!preg_match($this->pattern, $value)) {
      $this->addError($this->message);
    }
  }

}
