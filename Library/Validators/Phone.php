<?php

namespace Validators;

class Phone extends Validator {

  public $message = 'Не верный формат телефонного номера';
  public $allowEmpty = true;

  public function validateAttribute() {
    $value = $this->getAttributeValue();
    if ($this->allowEmpty && $this->isEmpty($value)) return;

    $value = preg_replace('#[\D]*#', '', $value);

    if (!preg_match('/^[0-9].{9,9}$/', $value)) {
      $this->addError($this->message);
    }
  }

}
