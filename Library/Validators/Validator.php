<?php

namespace Validators;

class Validator {

  public $model;
  public $attribute;
  public $message;
  public $allowEmpty = true;

  public static function createValidator($type, $model, $attribute, $options = []) {
    $validator = new $type;
    $validator->model = $model;
    $validator->attribute = $attribute;

    foreach ($options as $key => $value) {
      $validator->$key = $value;
    }

    return $validator;
  }

  protected function getAttributeValue() {
    return $this->model->{$this->attribute};
  }

  public function addError($message) {
    $attributeLabel = $this->model->getAttributeLabel($this->attribute);
    $this->model->addError($this->attribute, strtr($message, ['{attribute}' => $attributeLabel]));
  }

  protected function isEmpty($value, $trim = false) {
    return $value === null || $value === array() || $value === '' || $trim && is_scalar($value) && trim($value) === '';
  }

}
