<?php

namespace Validators;

class Required extends Validator {

  public $message = 'Заполните &laquo;{attribute}&raquo;';

  public function validateAttribute() {
    if ($this->isEmpty($this->getAttributeValue(), true)) {
      $this->addError($this->message);
    }
  }

}
