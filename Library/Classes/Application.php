<?php

namespace TestTask\Library;

class Application extends \Application {

  public static function getApplicationID() {
    return 'TestTask';
  }

  /**  @return Autoloader */
  public function getAutoloader() {
    if (!isset($this->_autoloader)) $this->_autoloader = new Autoloader();

    return $this->_autoloader;
  }

  /** @return FrontController    */
  public function getFronController() {
    if (!isset($this->_frontController)) $this->_frontController = FrontController::getInstance();

    return $this->_frontController;
  }

}
