<?php

namespace TestTask\Library\ARB;

trait SafeDelete {

  /** перехватчик стандарного удаления */
  public function delete() {
    $this->safeDelete();
  }

  /** реализовывает безопастное удаление */
  public function safeDelete() {
    $this->deleted = 'Y';
    $this->save();
  }

}
