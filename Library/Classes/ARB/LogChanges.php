<?php

namespace TestTask\Library\ARB;

trait LogChanges {

  private $_logChanges_snapshot = [];

  /** автоматически снять слепок после загрузки экземпляра модели */
  public function afterFind() {
    $this->logChangesTakeSnapshot();
    parent::afterFind();
  }

  /** автоматически записать лог измененых атрибутов */
  public function afterSave() {
    $this->logChangesSave();
    parent::afterSave();
  }

  /** запомнить старые значение атрибутов объекта */
  public function logChangesTakeSnapshot() {
    $this->_logChanges_snapshot = $this->getAttributes();
  }

  /** записать лог измененых атрибутов */
  public function logChangesSave() {

    $old = $this->_logChanges_snapshot;
    $new = $this->getAttributes();
    $record_id = $new[$this->_pk_name];

    # type of operation
    $type = 'update';

    # creaate
    if (!$old[$this->_pk_name]) {
      $type = 'create';
      $diff = json_encode($new, JSON_UNESCAPED_UNICODE);
    }

    # delete 
    if ($new['deleted'] === 'Y') {
      $type = 'safe_delete';
      $diff = null;
    }

    # update
    if ($type === 'update') {
      $diffArray = [];
      foreach ($old as $key => $value) {
        if ($value !== $new[$key]) $diffArray[$key] = [$value, $new[$key]];
      }
      if (!$diffArray) return;
      $diff = json_encode($diffArray, JSON_UNESCAPED_UNICODE);
    }

    # save
    $sql = "INSERT INTO `log_model_changes` (`type`, `table`, `record_id`, `diffs`) "
            . "VALUES ('{$type}', '{$this->_table_name}', {$record_id}, " . ($diff ? "'{$diff}'" : 'NULL') . ")";
    $this->_db->exec($sql);
  }

}
