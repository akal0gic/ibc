<?php

namespace TestTask\Library;

class RewriteRouter extends \RewriteRouter {

  public function route(\Request $request) {
    $controller = $action = 'Index';
    $params = [];

    if ('/' !== $request->getBaseUrl()) {
      $urlPaths = explode('/', rtrim($request->getBaseUrl(), '/'));
      if (isset($urlPaths[1])) $controller = $urlPaths[1];
      if (isset($urlPaths[2]) AND ( $urlPaths[2])) $action = $urlPaths[2];

      if (count($urlPaths) > 3) {
        $requestParams = array_slice($urlPaths, 3);

        for ($i = 0; $i < count($requestParams); $i += 2) {
          $params[$requestParams[$i]] = isset($requestParams[$i + 1]) ? $requestParams[$i + 1] : null;
        }
      }
    }

    $request->setControllerName(ucfirst($controller));
    $request->setActionName(ucfirst($action));
    $request->setParams($params);
  }

}
