<?php

namespace TestTask\Library;

class ActionController extends \ActionController {

  /**
   * Добавлен обработчик исключений.
   * @see \ActionController::dispatch()
   */
  public function dispatch($action) {

    $this->_initLayout();
    $this->_initView();
    $this->_init();
    ob_start();

    try {
      if (!method_exists($this, $action)) throw new \Exception('page not found', 404);

      $this->$action();
    } catch (\Exception $exc) {

      if (404 === $exc->getCode()) {
        $this->_view->setScriptName('Error/404.phtml');
        $this->_view->render();
        $content = ob_get_clean();
        return $this->outputLayout($content);
      }
    }

    if ($this->_view->enabled()) $this->_view->render();
    $content = ob_get_clean();
    $this->outputLayout($content);
  }

  /**
   * Рендерит Layout c $content
   * @param string $content -
   */
  private function outputLayout($content = '') {
    if ($this->_layout->enabled()) {
      ob_start();
      $this->_layout->setContent($content);
      $this->_layout->render();
      $content = ob_get_clean();
    }

    $this->_response->setContent($content);
  }

}
