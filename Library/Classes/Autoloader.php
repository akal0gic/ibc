<?php

namespace TestTask\Library;

class Autoloader extends \Autoloader {

  protected $_aliases = [];

  public function load($class_name) {
    # Расширеный автолоад по неймспейсам
    if (strpos($class_name, '\\') !== false) {
      return $this->loadClassUsingNameSpaceAlias($class_name);
    }

    # стандартный автолоад ibc
    parent::load($class_name);
  }

  private function loadClassUsingNameSpaceAlias($class_name) {
    # Обратный поиск объявленного алиаса (Task\Lib\Controller -> Task\Lib -> Task)
    $alias_parts = explode('\\', $class_name);
    $alias_posible_parts_count = $alias_current_parts_to_exam = count($alias_parts) + 1;

    while (--$alias_current_parts_to_exam) {
      $alias = implode('\\', array_slice($alias_parts, 0, $alias_current_parts_to_exam));
      if (isset($this->_aliases[$alias])) break;
    }

    # Поиск файла в дериктории закрепленной за алиасом
    $path_without_alias = implode(DIRECTORY_SEPARATOR, array_slice($alias_parts, $alias_current_parts_to_exam, $alias_posible_parts_count));
    $file_name = str_replace('\\', DIRECTORY_SEPARATOR, $path_without_alias);
    $file = $this->_aliases[$alias] . $file_name . '.php';
    if (!file_exists($file)) throw new \Exception('Unable to load "' . $class_name . '" looking in  "' . $file . '"');

    include $file;
  }

  public function setAlias($name, $path) {
    $this->_aliases[$name] = $path;
  }

  public function setAliases($aliases) {
    foreach ($aliases as $name => $path) {
      $this->setAlias($name, $path);
    }
  }

}
