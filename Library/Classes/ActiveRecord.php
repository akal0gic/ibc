<?php

namespace TestTask\Library;

class ActiveRecord extends Object {

  /** @var \PDO default connection */
  protected $_db;
  protected $_table_name;
  protected $_pk_name = 'id';
  protected $_new_record = true;

  function __construct() {
    $this->_db = \Registry::get('db');
  }

  public function rules() {
    return [];
  }

  public function isNewRecord() {
    return $this->_new_record;
  }

  public function afterFind() {
    $this->_new_record = false;
  }

  public function findByPk($pk) {
    $sql = "SELECT * FROM `{$this->_table_name}` WHERE {$this->_pk_name}=:" . $this->_pk_name;

    $pdo = $this->_db->prepare($sql);
    $pdo->bindParam(':' . $this->_pk_name, $pk);
    $pdo->execute();

    $result = $pdo->fetch(\PDO::FETCH_ASSOC);

    $this->setAttributes($result);
    $this->afterFind();

    return (bool) $result;
  }

  /**
   * Сохраняет объект
   * @param bool $runValidation - Валидировать данные перед сохранением
   */
  public function save($runValidation = true) {
    if ($runValidation && !$this->validate()) return false;

    $this->beforeSave();
    $this->{($this->isNewRecord()) ? 'insert' : 'update'}();
    $this->afterSave();

    return true;
  }

  public function beforeSave() {
    return false;
  }

  public function afterSave() {
    
  }

  public function insert() {
    $data = $this->getAttributes();

    $sql_fields = '`' . implode('`, `', array_keys($data)) . '`';
    $sql_placeholders = ':' . implode(', :', array_keys($data));

    $sql = "INSERT INTO {$this->_table_name} ($sql_fields) VALUES ({$sql_placeholders})";

    $pdo = $this->_db->prepare($sql);
    $this->PDOBindParamsHelper($pdo, $data);
    $pdo->execute();

    $this->{$this->_pk_name} = $this->_db->lastInsertId();
  }

  public function update() {
    $data = $this->getAttributes();

    $fields = '';
    foreach ($data as $name => $value) {
      $fields.= $name . '=:' . $name . ', ';
    }
    $fieldsToUpdate = rtrim($fields, ', ');

    $sql = "UPDATE {$this->_table_name} SET {$fieldsToUpdate} WHERE {$this->_pk_name}=:" . $this->_pk_name;

    $pdo = $this->_db->prepare($sql);
    $this->PDOBindParamsHelper($pdo, $data);
    $pdo->execute();
  }

  private function PDOBindParamsHelper($pdo, $data) {
    foreach ($data as $name => &$value) {
      $pdo->bindParam(':' . $name, $value);
    }

    return $pdo;
  }

}
