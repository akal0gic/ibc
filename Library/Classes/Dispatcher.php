<?php

namespace TestTask\Library;

class Dispatcher extends \Dispatcher {

  public function getControllerClass($controllerName) {
    return Application::getApplicationID() . '\Controllers\\' . ucfirst($controllerName) . 'Controller';
  }

  public function getControllerFunction($actionName) {
    return $actionName . 'Action';
  }

  public function dispatch(\Request $request, \Response $response) {
    $controller = $this->getController($request->getControllerName());
    $controller->setRequest($request);
    $controller->setResponse($response);

    $function = $this->getControllerFunction($request->getActionName());
    
    $controller->dispatch($function);
  }

}
