<?php

namespace TestTask\Library;

class HttpRequest extends \HttpRequest
{

  protected $_baseUrl = null;

  public function __construct($params = null)
  {

    $this->setParams($_REQUEST);
    if (is_array($params)) $this->setParams($params);

    $this->setBaseUrl($_SERVER['REQUEST_URI']);
  }

}
