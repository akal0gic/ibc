<?php

namespace TestTask\Library;

/**
 * Базовый объект реализующий работу с атрибутами экземпляра класса
 */
abstract class Object {

  protected $_attributes = [];

  /** @var array активные валидаторы */
  private $_validators = [];

  /** @var array ошибки валидации (атрибут => массив ошибок) */
  private $_errors = [];

  /**
   * получение значение атрибута 
   */
  public function __get($name) {
    if (!array_key_exists($name, $this->_attributes)) throw new \Exception('Unknown attribute ' . $name);

    return $this->_attributes[$name];
  }

  /**
   * установка значение атрибута
   */
  public function __set($name, $value) {
    if (!array_key_exists($name, $this->_attributes)) throw new \Exception('Unknown attribute ' . $name);

    $this->_attributes[$name] = $value;
  }

  /**
   * объявление атрибута
   */
  protected function defineAttribute($name, $value = null) {
    $this->_attributes[$name] = $value;
  }

  /**
   * объявление атрибутов 
   * @array $attributes - может принимать значения name-value или names
   */
  protected function defineAttributes($attributes) {

    foreach ($attributes as $name => $value) {
      if (is_int($name)) {
        $this->_attributes[$value] = null;
      } else {
        $this->_attributes[$name] = $value;
      }
    }
  }

  /**
   * Возвращает значение всех атрибутов
   */
  public function getAttributes() {
    return $this->_attributes;
  }

  /**
   * Массово устанавливает значение для всех ранее объявленых атррибутов, игнорируя не объявленые
   * @param array $values (name => value)  
   */
  public function setAttributes($values) {
    if (!$values || !is_array($values)) return;

    foreach ($values as $name => $value) {
      if ($value === '') $value = null;
      
      if (array_key_exists($name, $this->_attributes)) {
        $this->$name = $value;
      }
    }
  }

  /**
   * @return str Возвращает ярлыки атрибутов.
   */
  public function attributeLabels() {
    return [];
  }

  /**
   * @return str Возвращает ярлык атрибута если он указан. 
   */
  public function getAttributeLabel($attribute) {
    $labels = $this->attributeLabels();
    return isset($labels[$attribute]) ? $labels[$attribute] : $attribute;
  }

  /** Правила валидации атрибутов объекта */
  abstract public function rules();

  /**
   * Проверяет все атрибуты на соответсвие правилам установленным в @see rules()
   * @return bool
   */
  public function validate() {

    # при первом обращении создать валидаторы
    if (!$this->_validators) {
      if (!($rules = $this->rules())) throw new \Exception('Rules not pressent');
      foreach ($this->rules() as $rule) {
        $this->addValidator($rule);
      }
    }

    foreach ($this->_validators as $validator) {
      $validator->validateAttribute();
    }

    return !$this->hasError();
  }

  /**
   * Добавляет новый валидатор @see validate
   *  
   * @param type $rule
   * @throws \Exception
   */
  private function addValidator($rule) {

    if (!is_array($rule) || !isset($rule[0], $rule[1])) throw new \Exception('Validation rule wrong format');
    if (!class_exists($rule[1], true)) throw new \Exception('Wrong validator "' . $rule[1] . '"');

    # добавляем валидаторы для каждого указанного в правиле атрибуте
    $attributes = explode(',', $rule[0]);
    foreach ($attributes as $attribute) {
      $attribute = trim($attribute);

      if (!array_key_exists($attribute, $this->_attributes))
          throw new \Exception('Wrong attribute (' . $attribute . ') to validate');

      $this->_validators[] = \Validators\Validator::createValidator($rule[1], $this, $attribute, array_slice($rule, 2));
    }
  }

  /**
   * Добавляет текс ошибки к атрибуту не прошедшему валидацию 
   */
  public function addError($attribute, $message) {
    if (!array_key_exists($attribute, $this->_errors)) $this->_errors[$attribute] = [];

    $this->_errors[$attribute][] = $message;
  }

  /**
   * @return array  - Список ошибок валидации атрибута
   */
  public function getError($attribute) {
    return (isset($this->_errors[$attribute])) ? $this->_errors[$attribute] : [];
  }

  /**
   * Сообшит о наличии ранее найденых ошибочных значениях атрибутов
   * 
   * @param null|mixed $attribute
   * @return bool
   */
  public function hasError($attribute = null) {
    return (bool) (($attribute) ? count($this->getError($attribute)) : count($this->_errors));
  }

}
