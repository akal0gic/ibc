<?php

namespace TestTask\Library;

class FrontController extends \FrontController {

  public function run() {
    $request = $this->getRequest();
    $response = $this->getResponse();

    $this->getRouter()->route($request);
    $this->getDispatcher()->dispatch($request, $response);

    $response->send();
  }

  /** @return HttpRequest HttpRequest */
  public function getRequest() {
    if (!isset($this->_request)) $this->_request = new HttpRequest();

    return $this->_request;
  }

  /** @return RewriteRouter RewriteRouter */
  public function getRouter() {
    if (!isset($this->_router)) $this->_router = new RewriteRouter();

    return $this->_router;
  }

  /** @return Dispatcher Dispatcher */
  public function getDispatcher() {
    if (!isset($this->_dispatcher)) $this->_dispatcher = new Dispatcher();

    return $this->_dispatcher;
  }

}
