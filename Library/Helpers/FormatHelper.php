<?php

namespace Helpers;

/** Хелпер формата строк */
class FormatHelper {

  public static $dayOfWeek = [1 => 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];
  public static $dayOfWeekCase = [1 => 'понедельник', 'вторник', 'среду', 'четверг', 'пятницу', 'субботу', 'воскресенье'];
  public static $month = [1 => 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
  public static $monthCase = [1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

  /** Приводит телефонный номер к удобочитаемому формату */
  public static function phone($phone) {
    $phoneLen = strlen($phone);
    if (10 == $phoneLen) {
      return '+7 (' . $phone[0] . $phone[1] . $phone[2] . ') ' . $phone[3] . $phone[4] . '-' . $phone[5] . $phone[6] . '-' . $phone[7] . $phone[8] . $phone[9];
    }
    return $phone;
  }

  /**
   * Форматирует дату согластно переданому шаблону
   * 
   * @param int|str|\DateTime $date - время
   * @param str $format - расширяет стандартный формат @see strftime()
   * 
   * @example Месяц в формате "%B" в разных локалях может быть "января" или "Январь". Вместо "%B" следует использовать "%month" или "%monthCase"
   * @example Дни недели "%dayOfWeek" и "%dayOfWeekCase"
   */
  public static function date($date, $format = '%d %monthCase %Y в %H:%M') {

    if ($date instanceof \DateTime) $dt = $date;
    else {  
      try {
        $dt = (!is_numeric($date) ? new \DateTime($date) : (new \DateTime())->setTimestamp($date));
      } catch (\Exception $exc) {
        return $date;
      }
      if (!$dt->getTimestamp()) return $date;
    }
    $m = (int) $dt->format('m');

    $format = strtr($format, [
        '%monthCase' => self::$monthCase[$m],
        '%month' => self::$month[$m],
    ]);

    $result = strftime($format, $dt->getTimestamp());

    return $result;
  }

}
