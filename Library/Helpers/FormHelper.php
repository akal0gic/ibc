<?php

namespace Helpers;

/** Хелпер форм */
class FormHelper {

  public $model;
  public $modelName;
  public $template;

  public function __construct($config) {

    foreach ($config as $name => $value) {
      $this->{$name} = $value;
    }
  }

  public function encode($text) {
    
    return htmlspecialchars($text, ENT_QUOTES);
  }

  public function inputNameValue($attribute) {
    $model = $this->model;
    $value = $this->encode($model->{$attribute});

    return ' name="' . $this->modelName . '[' . $attribute . ']" value="' . $value . '" ';
  }

  public function error($attribute) {
    $model = $this->model;
    if (!$model->hasError($attribute)) return;
    $errors = join('</li><li>', $model->getError($attribute));

    return strtr($this->template['error'], ['{content}' => $errors]);
  }

}
