<?php

setlocale(LC_ALL, 'ru_RU.UTF-8', 'Rus');

$path = realpath(__DIR__ . '/..') . '/';
chdir($path);

include_once $path . 'Vendor/Classes/Application.php';
include_once $path . 'Vendor/Classes/Autoloader.php';

include_once $path . 'Library/Classes/Autoloader.php';
include_once $path . 'Library/Classes/Application.php';

$application = new \TestTask\Library\Application();

$application->getAutoloader()->addDirectories([
    $path,
    $path . 'Vendor/Classes',
    $path . 'Vendor/Plugins',
]);

$application->getAutoloader()->setAliases([
    'TestTask' => $path,
    'TestTask\Library' => $path . 'Library/Classes/',
    'TestTask\Controllers' => $path . 'Controllers/',
    'TestTask\Models' => $path . 'Models/',
    'TestTask\Views' => $path . 'Views/',
    'Validators' => $path . 'Library/Validators/',
    'Helpers' => $path . 'Library/Helpers/',
]);

$application->getBootstrap()->bootstrap();
$application->run();
