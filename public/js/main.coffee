$(document).ready ->

	# настройки датапикера
	$.datepicker.regional['ru'] =
	  closeText: 'Закрыть'
	  prevText: '&#x3c;Пред'
	  nextText: 'След&#x3e;'
	  currentText: 'Сегодня'

	  monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
	  monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
	  dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
	  dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
	  dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
	  dateFormat: 'yy-mm-dd'

	  firstDay: 1
	  isRTL: false
	$.datepicker.setDefaults $.datepicker.regional['ru']

	# иницализация датапикера
	$(".js-datapicker").datepicker
		changeMonth: true
		changeYear: true
		regional: ['ru']

	# иницализация jquery.maskedinput.min.js
	$(".js-maskedinput").each (index, dom_element) -> 
		mask = $(dom_element).attr 'mask'
		$(dom_element).mask mask, {placeholder: ''}

	return