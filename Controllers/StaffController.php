<?php

namespace TestTask\Controllers;

class StaffController extends \TestTask\Library\ActionController {

  public function _init() {
    $this->_layout->LocalMenu->add('<a href="/staff">список сотрудников</a>');
    $this->_layout->LocalMenu->add('<a href="/staff/add">добавить нового сотрудника</a>');
    $this->_layout->LocalMenu->add('<a href="/staff/changes">история изменений</a>');
  }

  /** Cписок сотрудников */
  public function indexAction() {
    $model = $this->initModel();

    $this->_view->staff_list = $model->search();
  }

  /** Добавить нового сотрудника */
  public function addAction() {
    $model = $this->initModel();

    if (($request = $this->_request->getParam('Staff', false))) {
      $model->setAttributes($request);

      if ($model->save()) {
        header('location: /staff/edit/id/' . $model->id);
      }
    }

    $this->_view->model = $model;
  }

  /** Редактировать данные сотрудника */
  public function editAction() {
    $model = $this->initModel($this->_request->getParam('id'));

    if (($request = $this->_request->getParam('Staff', false))) {
      $model->setAttributes($request);

      if ($model->save()) {
        header('location: /staff/edit/id/' . $model->id);
      }
    }

    $this->_view->model = $model;
    $this->_view->changes = \TestTask\Models\LogModelChangesGateway::get('staff', $model->id);
  }

  /** Безопасное удаление сотрудника */
  public function deleteAction() {
    $model = $this->initModel($this->_request->getParam('id'));
    $model->delete();

    header('location: /staff/');
  }

  /** Общий список изменений по разделу */
  public function changesAction() {
    $this->_view->model = $this->initModel();
    $this->_view->changes = \TestTask\Models\LogModelChangesGateway::get('staff', false);
  }

  /**
   * Загружает модель по первичному ключу.
   * @return \TestTask\Models\Staff
   * @throws \Exception - если модель не найдена.
   */
  private function initModel($id = false) {
    $model = new \TestTask\Models\Staff;

    if ($id && !$model->findByPk($id)) throw new \Exception('page not found', 404);

    return $model;
  }

}
